module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  eleventyConfig.addPassthroughCopy({ "static/outputs": "/outputs" });

  eleventyConfig.addCollection("sortedByOrder", function (collectionApi) {
    return collectionApi.getAll().sort((a, b) => {
      if (a.data.order > b.data.order) return 1;
      else if (a.data.order < b.data.order) return -1;
      else return 0;
    });
  });
  eleventyConfig.addCollection("allDemos", collectionApi => {
    return collectionApi.getFilteredByGlob("src/content/**/*.md").sort((a, b) => a.data.part - b.data.part);
  });
  //import all flax plugins
  // eleventyConfig.addPlugin(require('./flax-plugins'));

  eleventyConfig.addPlugin(require('./plugins/imgSlideshow.js'));




  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};