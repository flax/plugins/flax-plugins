const slugify = require('slugify')
module.exports = (eleventyConfig) => {
  eleventyConfig.addShortcode(
    'imgSlideshow', 
    (imgArr) => {
      if(!imgArr) return;

      let str= `
      <style>
      /*

        Self-referenced links, #permalinks, native lazy loading.

        Each link pseudo-elements bleed 50% into the previous and next slide, acting as prev/next buttons, supplementing scroll/swipe, Tab key (focus), and keyboard arrows.

      */
      html,
      body {
        height: 100%;
      }
      
      .slideshow {
        height: 100%;
        overflow-x: scroll;
        scroll-snap-type: x mandatory;
        overscroll-behavior-x: contain;
        display: flex;
        gap: 4vw;
      }
      
      /* transition on :hover only so they don’t fire when opening a #permalink */

      .slideshow:hover {
        scroll-behavior: smooth;
        /* auto for no transition on click/tab */
      }
      
      .slideshow figure {
        position: relative;
        scroll-snap-align: start;
        flex-shrink: 0;
        width: 100%;
        height: 100%;
        align-items: center;
        justify-content: center;
      }
      
      .slideshow figure a {
        flex-shrink: 0;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        -webkit-tap-highlight-color: transparent;
      }
      
      .slideshow a img {
        max-width: 100%;
        max-height: 100%;
        object-fit: contain;
        z-index: 1; /* cover the loading spinner */
      }
      
      .slideshow a:focus-visible {
        outline: 0;
      }
      
      /* Prev / Next */
      
      .slideshow a:before,
      .slideshow a:after {
        z-index: 2; /* cover the current image <a> */
        content: "";
        display: block;
        position: absolute;
        top: 0;
        bottom: 0;
        width: calc(50% + 4vw); /* 50vw + 4vw gap */
      }
      
      .slideshow a:before {
        left: calc(-1 * calc(50% + 4vw));
      }
      
      .slideshow a:after {
        right: calc(-1 * calc(50% + 4vw));
      }
      
      .slideshow :first-child a:before,
      .slideshow :last-child a:after {
        display: none;
      }
      
      /* -- Loading spinner -- */
      
      @keyframes spin {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
      
      .slideshow figure::after {
        content: "";
        height: 2em;
        width: 2em;
        animation: spin 0.8s infinite linear;
        border: 1px solid;
        border-right-color: transparent;
        border-radius: 50%;
        display: block;
        position: absolute;
        top: calc(50% - 1em);
        right: calc(50% - 1em);
        opacity: 0.4;
      }
      
      /* Counter */
      
      .slideshow {
        counter-reset: currentStep 0 remainder 0 totalStep 0;
      }
      
      .slideshow figure {
        counter-increment: totalStep;
      }
      
      .slideshow figure::before {
        content: "";
        counter-increment: currentStep;
      }
      
      .slideshow figure:focus-within ~ figure::before {
        counter-increment: remainder;
      }
      
      .slideshow:hover figure:focus-within ~ .counter {
        z-index: 1;
        font-variant-numeric: tabular-nums;
        position: absolute;
        bottom: 1em;
        right: 1.5em;
        text-shadow: 1px 1px 1px #000;
      }
      
      .slideshow .counter::before {
        font-size: 0.9em;
        content: counter(currentStep) " / " counter(totalStep);
      }
      </style>
      
      <div class="slideshow">`;
      imgArr.forEach(img => {
        if(img.src){
          let id =  img.name? slugify(img.name) : Math.random() * 10 **16
          let alt = img.name? img.name : ''
          str = `${str}
          <figure>
            <a id="${id}" href="#${id}">
              <img loading="lazy" src="${img.src}" alt="${alt}" title="${alt}">
            </a>
          </figure>`
        }
      });

      str = `${str} <div class="counter"></div></div>`
      return str;
    }
)}; 